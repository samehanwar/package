<?php
namespace Objectify\ObjectRelation;

use Illuminate\Support\ServiceProvider;

class ObjectRelationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

      $this->loadRoutesFrom(__DIR__.'/routes.php');
      $this->loadViewsFrom(__DIR__.'/views', 'objectify');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->make('Objectify\ObjectRelation\RelationBetweenObjectsController');
    }
}
