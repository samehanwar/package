<?php
namespace Objectify\ObjectRelation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

/**
 *
 */
class RelationBetweenObjectsController extends Controller
{

    public $category = [
    							['id' => 1, "name" => "category 1"] ,
    							['id' => 2, "name" => "category 2"]
    						];

    public $products = [
    							['id' => 1, "name" => "item 1" , "price" =>50 , "category_id" => "1"],
    							['id' => 2, "name" => "item 2" , "price" => 60 , "category_id" => "2" ]
    						];

    public $images = [
    						 ['product_id' => 1, "name" => "img1.jpg"],
    						 ['product_id' => 2, "name" => "img2.jpg"]
    					 ];

    // public function __construct($category, $products, $images){
    //     $this->$category = $category;
    //     $this->$products = $products;
    //     $this->$images = $images;
    // }


    public function index()
    {

      // $FormData[] = [ $this->category,  $this->products, $this->images];
      $FormData = Storage::disk('local')->exists('data.txt') ? Storage::disk('local')->get('data.txt') : [];
      $output = $this->getObject();

      return view('objectify::relation', ['inputs' => $FormData, 'output' => $output]);

    }

    public function getObject()
    {

      $subject1 = $this->relatedObjectsOnKeys($this->category, $this->products, 'id', 'category_id');
      $subject2 = $this->relatedObjectsOnKeys($this->products, $this->images, 'id', 'product_id');

      return json_encode($this->RelateParentToChild($subject1, $subject2, $this->products, $this->images), JSON_PRETTY_PRINT);

    }

    function RelateParentToChild($subject1, $subject2, $parentObject, $childObject){

       for($x=0; $x < func_num_args()/2; $x++){
          $subject1[$x][$this->getVariableName($parentObject)][$this->getVariableName($childObject)] = $subject2[$x][$this->getVariableName($childObject)];
       }
        return $subject1;
    }

    protected function relatedObjectsOnKeys($array1, $array2, $array1Key, $array2Key){
         $resultObject = [];

         foreach ($array1 as $key => $value) {
             $resultObject[$key] = $value;

             foreach($array2 as $item) {
               if($value[$array1Key] == $item[$array2Key]){

                 $resultObject[$key][$this->getVariableName($array2)] = $item;
                 unset($resultObject[$key][$this->getVariableName($array2)][$array2Key]);
               }

             }
         }

         return $resultObject;
    }



   private function getVariableName($var) {

     $classProps =  get_object_vars($this);
     foreach($classProps as $var_name => $value) {
         if ($value === $var) {
             return $var_name;
         }
     }

     return false;
   }

}
